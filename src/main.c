#define NRFX_APP_CONFIG_LOG_ENABLED 1
#define NRFX_APP_CONFIG_LOG_LEVEL 4
#define NRFX_NFCT_CONFIG_LOG_ENABLED 1
#define NRFX_NFCT_CONFIG_LOG_LEVEL 4
#define NRFX_I2S_CONFIG_LOG_ENABLED 1
#define NRFX_I2S_CONFIG_LOG_LEVEL 4
#define NRFX_TIMER_CONFIG_LOG_ENABLED 1
#define NRFX_TIMER_CONFIG_LOG_LEVEL 4

#include <device.h>
#include <devicetree.h>
#include <drivers/gpio.h>
#include <drivers/i2s.h>
#include <errno.h>
#include <inttypes.h>
#include <kernel.h>
#include <math.h>
#include <stddef.h>
#include <string.h>
#include <sys/byteorder.h>
#include <sys/printk.h>
#include <sys/util.h>
#include <zephyr.h>
#include <zephyr/types.h>

#define NRFX_LOG_MODULE APP
#include <nrfx_log.h>

#include <hal/nrf_gpio.h>
#include <nrf.h>
#include <nrfx_i2s.h>
#include <nrfx_nfct.h>
#include <nrfx_timer.h>

#include "coin.h"
#include "rick.h"

#define SD_MODE_NODE DT_ALIAS(led0)

#if DT_NODE_HAS_STATUS(SD_MODE_NODE, okay)
#define SD_MODE_GPIO_LABEL DT_GPIO_LABEL(SD_MODE_NODE, gpios)
#define SD_MODE_GPIO_PIN NRF_GPIO_PIN_MAP(0, 14)
#define SD_MODE_GPIO_FLAGS DT_GPIO_FLAGS(SD_MODE_NODE, gpios)
#else
#define SD_MODE_GPIO_LABEL ""
#define SD_MODE_GPIO_PIN 0
#define SD_MODE_GPIO_FLAGS 0
#endif

#define I2S_TX_NODE DT_NODELABEL(i2s_0)

#define SAMPLE_FREQUENCY 22050
#define SAMPLE_BIT_WIDTH 16
#define NUMBER_OF_CHANNELS 2
#define INITIAL_BLOCKS 2
#define TIMEOUT SYS_FOREVER_MS

#define BLOCK_SIZE 1024
#define BLOCK_COUNT (INITIAL_BLOCKS + 2)
static K_MEM_SLAB_DEFINE(mem_slab, BLOCK_SIZE, BLOCK_COUNT, 4);

/*
#define PIN_SDOUT NRF_GPIO_PIN_MAP(0, 4)
#define PIN_SCK NRF_GPIO_PIN_MAP(0, 7)
#define PIN_SD_MODE NRF_GPIO_PIN_MAP(0, 8)
#define PIN_LRCK NRF_GPIO_PIN_MAP(0, 12)
*/

static size_t clip_index = 0;

static size_t process_block_data(int16_t *mem_block, uint32_t number_of_samples) {
  //printk("process_block_data %d\n", clip_index);
  if (clip_index + number_of_samples < sizeof(rick)) {
    memcpy(mem_block, rick + clip_index, number_of_samples);
    clip_index += number_of_samples;
    return number_of_samples;
  } else {
    size_t remainder = sizeof(rick) - clip_index;
    memcpy(mem_block, rick + clip_index, remainder);
    memset(mem_block + remainder, 0, number_of_samples - remainder);
    clip_index = 0;
    return remainder;
  }
}

static bool configure_streams(const struct device *i2s_dev_rx, const struct device *i2s_dev_tx, const struct i2s_config *config) {
  int ret;

  ret = i2s_configure(i2s_dev_tx, I2S_DIR_TX, config);
  if (ret < 0) {
    printk("Failed to configure TX stream: %d\n", ret);
    return false;
  }

  return true;
}

static bool prepare_transfer(const struct device *i2s_dev_rx, const struct device *i2s_dev_tx) {
  printk("Prepare transfer\n");
  int ret;

  for (int i = 0; i < INITIAL_BLOCKS; ++i) {
    void *mem_block;

    ret = k_mem_slab_alloc(&mem_slab, &mem_block, K_NO_WAIT);
    if (ret < 0) {
      printk("Failed to allocate TX block %d: %d\n", i, ret);
      return false;
    }

    memcpy(mem_block, rick, BLOCK_SIZE);
    clip_index = BLOCK_SIZE;

    ret = i2s_write(i2s_dev_tx, mem_block, BLOCK_SIZE);
    if (ret < 0) {
      printk("Failed to write block %d: %d\n", i, ret);
      return false;
    }
  }

  return true;
}

static bool trigger_command(const struct device *i2s_dev_rx, const struct device *i2s_dev_tx, enum i2s_trigger_cmd cmd) {
  int ret;

  ret = i2s_trigger(i2s_dev_tx, I2S_DIR_TX, cmd);
  if (ret < 0) {
    printk("Failed to trigger command %d on TX: %d\n", cmd, ret);
    return false;
  }

  return true;
}

void main(void) {
  int err;
  const struct device *i2s_dev_tx = DEVICE_DT_GET(I2S_TX_NODE);
  struct i2s_config config;
  config.word_size = SAMPLE_BIT_WIDTH;
  config.channels = NUMBER_OF_CHANNELS;
  config.format = I2S_FMT_DATA_FORMAT_I2S;
  config.options = I2S_OPT_BIT_CLK_MASTER | I2S_OPT_FRAME_CLK_MASTER | I2S_OPT_BIT_CLK_CONT;
  config.frame_clk_freq = SAMPLE_FREQUENCY;
  config.mem_slab = &mem_slab;
  config.block_size = BLOCK_SIZE;
  config.timeout = TIMEOUT;

  printk("I2S echo sample\n");

  const struct device *gpio = device_get_binding(SD_MODE_GPIO_LABEL);

  if (gpio == NULL) {
    printk("Could not bind %s.\n", SD_MODE_GPIO_LABEL);
    return;
  }

  err = gpio_pin_configure(gpio, SD_MODE_GPIO_PIN, GPIO_OUTPUT);
  if (err) {
    printk("Could not configure %d.\n", SD_MODE_GPIO_PIN);
    return;
  }

  if (!device_is_ready(i2s_dev_tx)) {
    printk("%s is not ready\n", i2s_dev_tx->name);
    return;
  }

  if (!configure_streams(NULL, i2s_dev_tx, &config)) {
    printk("Failed to configure\n");
    return;
  }

  printk("waiting...\n");
  while (true) {
    if (!prepare_transfer(NULL, i2s_dev_tx)) {
      printk("Failed to prepare\n");
      return;
    }

    if (!trigger_command(NULL, i2s_dev_tx, I2S_TRIGGER_START)) {
      printk("error with I2S_TRIGGER_START\n");
      return;
    }
    printk("Stream started\n");
    gpio_pin_set(gpio, SD_MODE_GPIO_PIN, 1);

    while (true) {
      void *mem_block;
      uint32_t block_size;
      int ret;

      ret = k_mem_slab_alloc(&mem_slab, &mem_block, K_FOREVER);
      if (ret < 0) {
        printk("Failed to allocate TX block %d\n", ret);
        break;
      }

      block_size = process_block_data(mem_block, BLOCK_SIZE);
      ret = i2s_write(i2s_dev_tx, mem_block, BLOCK_SIZE);
      //ret = i2s_buf_write(i2s_dev_tx, rick, BLOCK_SIZE);
      if (ret < 0) {
        switch (ret) {
        case -EIO:
          printk("Failed to write data: EIO\n");
          break;
        case -EBUSY:
          printk("Failed to write data: EBUSY\n");
          break;
        case -EAGAIN:
          printk("Failed to write data: EAGAIN\n");
          break;
        case -ENOMSG:
          printk("Failed to write data: ENOMSG\n");
          break;
        default:
          printk("Failed to write data: %d\n", ret);
          break;
        }
        k_mem_slab_free(&mem_slab, &mem_block);
        break;
      }

      //clip over
      if (block_size < BLOCK_SIZE) {
        printk("Clip over\n");
        break;
      }
    }

    if (!trigger_command(NULL, i2s_dev_tx, I2S_TRIGGER_DROP)) {
      printk("error with I2S_TRIGGER_DROP\n");
      return;
    }

    printk("Streams stopped\n");
    //gpio_pin_set(gpio, SD_MODE_GPIO_PIN, 0);
    k_sleep(K_SECONDS(5));
  }
  printk("End of main\n");
}
